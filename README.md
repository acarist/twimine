Welcome to Twimine!
===================



Hey! This is **Twimine**. Twitter data miner with python. **Twimine** a hobby project. If you contribute this project, here are useful information :

----------


Installation
-------------

First of all you clone this repo :
```
$ git clone https://acarist@bitbucket.org/acarist/twimine.git
```

After cloning repo you must setup pip and virtualenv :
```bash
#install pip
$ sudo easy_install pip
#install virtualenv
$ pip install virtualenv
```
Go to project folder and activate virtual environment :
```bash
$ cd twimine
$ virtualenv .
$ source bin/activate
# (twimine)$  <--- you will see this
```
Now install requirement with pip :
```bash
(twimine)$ pip install -r req.txt
```

**We are ready to action!**

What am i did so far?
-------------------

#### Twitter Tweepy Client

**twitter_client.py** is the main file for connect twitter api. Inside this file there are twitter token,secret etc. It returns tweepy.API object.

#### Get All Tweets from Given Username

**get_all_tweets.py** runs in command line with single argument and get all tweets given username and save to file. Filename convention : user_timeline_{GIVEN_USERNAME}.jsonl . Jsonl means every tweets is json formatted lines. Usage example :

    $ python get_all_tweets.py username

#### Get Live Stream from Given Hashtag

**twitter_streaming.py ** runs in command line with multiple  argument and get live stream tweets given hashtag and save to file. Filename convention : stream_{GIVEN_HASHTAG}.jsonl. (there is a turkish char bug!)
Usage example :

    # python twitter_streaming.py \#hashtag1 \#hashtag2



#### Get Hashtag Frequency

**twitter_hashtag_frequency.py ** runs in command line with json file  argument and extracts the hashtags from a user timeline, producing a list of the most common ones.
Usage example :

    $ python twitter_hashtag_frequency.py user_timeline_username.jsonl


Sample Output :
```
saas: 49
bigdata: 43
design: 39
datascience: 18
ux: 15
dataviz: 13
coding: 12
ui: 10
webdesign: 9
marketing: 6
istanbul: 6
graphicdesign: 5
data: 5
startup: 5
analytics: 5
sea: 4
designers: 4
business: 4
apps: 4
mobileapps: 3
```

#### Get Hashtag Stats

**twitter_hashtag_stats.py ** runs in command line with json file  argument and give us an overview of how hashtags are used by user.
Usage example :

    $ python twitter_hashtag_stats.py user_timeline_username.jsonl


Sample Output :
```
1373 tweets without hashtags (42.91%)
1827 tweets with at least one hashtag (57.09%)
1029 tweets with 1 hashtags (32.16% total, 56.32% elite)
585 tweets with 2 hashtags (18.28% total, 32.02% elite)
181 tweets with 3 hashtags (5.66% total, 9.91% elite)
29 tweets with 4 hashtags (0.91% total, 1.59% elite)
2 tweets with 5 hashtags (0.06% total, 0.11% elite)
1 tweets with 7 hashtags (0.03% total, 0.05% elite)
```

####  Get Mention Frequency

**twitter_mention_frequency.py  ** runs in command line with json file  argument and give us mention frequency (you dont say!)
Usage example :

    $ python twitter_mention_frequency.py  user_timeline_username.jsonl


Sample Output :
```
UcaR_M: 67
s0lukciyerli: 31
worldoftanks_en: 20
Sevdali: 19
acarist: 16
chilidaisy: 16
BilgeAmca: 13
ceyhunyilmazz: 13
nekrofilzombi: 8
shaylaprice: 8
serdargokalp: 7
ceyhunyilmaz: 7
mustafavelioglu: 7
MuratAykul: 6
kaansezyum: 6
serkancura: 6
fuatavni: 5
mserdark: 5
lolieon: 5
```


#### Get Term Frequency
Analyse term frequency of given jsonl user tweets file. For now just support the english nltk library. If you want use this script, you must downlad nltk data. For information [click this](http://www.nltk.org/data.html).

Usage example :

    $ python twitter_term_frequency.py  user_timeline_username.jsonl


Sample Output :
```
:)  :  298
bu  :  140
bir  :  122
var  :  110
bi  :  109
ne  :  91
data  :  86
ben  :  80
için  :  79
çok  :  70
```

Additional Info:
The script takes a command-line argument for the .jsonl file to analyze. It initializes TweetTokenizer used for tokenization, and then defines a list of stop words. Such list is made up of common English stop words coming from NLTK, as well as punctuation symbols defined in string.punctuation. To complete the stop word list, we also include the 'rt', 'via', and '...' tokens (a single-character Unicode symbol for a horizontal ellipses).

Contribution
-------------------
If you want contribute this repo you must push your branch with your username and make pull request to master.

License
-------------------
This software is published under the GNU Public License.